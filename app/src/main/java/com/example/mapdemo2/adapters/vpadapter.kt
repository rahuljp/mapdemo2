package com.example.mapdemo2.adapters

import android.app.ActionBar
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.WindowManager
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import androidx.core.view.marginTop
import androidx.fragment.app.ListFragment
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mapdemo2.ListFragmentDirections

import com.example.mapdemo2.R
import com.example.mapdemo2.Screens.Model.CityData
import kotlinx.android.synthetic.main.vplist.view.*

class vpadapter(list : ArrayList<CityData> , navController: NavController) : RecyclerView.Adapter<vpadapter.VH>(){
    var list: ArrayList<CityData> = list
    lateinit var context : Context
    var controller=navController

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var view = itemView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        context=parent.context
        var view=LayoutInflater.from(context).inflate(R.layout.vplist,parent,false)
        var params=LinearLayout.LayoutParams(MATCH_PARENT,WRAP_CONTENT)
        params.setMargins(20,20,20,0)
        view.cv.layoutParams= params
        return VH(view)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        var view=holder.view
       var pos=holder.adapterPosition
        var data=list[pos]
        view.title2.text=data.title
        view.dist.text=data.distance
        view.desc.text=data.disc
        Glide.with(context).load(data.image).into(view.logo)
        view.setOnClickListener{
            var action=ListFragmentDirections.actionListFragmentToMapsFragment(pos)
            controller.navigate(action)
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }
}