package com.example.mapdemo2.ViewModels

import android.content.ClipData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder

import androidx.paging.PagedList

import android.content.ClipData.Item
import android.util.Log

import androidx.paging.PageKeyedDataSource

import androidx.lifecycle.LiveData
import com.example.mapdemo2.DataSources.CityDataSourceFactory
import com.example.mapdemo2.Network.Models.Data


class CityViewModel() : ViewModel() {
    //creating livedata for PagedList  and PagedKeyedDataSource
    var cityPagedList: LiveData<PagedList<Data?>>? = null
    var liveDataSource: LiveData<PageKeyedDataSource<Integer, Data>>? = null

    //constructor
    init {
        Log.d("abcdefgh","in viewmodel")
        //getting our data source factory
        val cityDataSourceFactory = CityDataSourceFactory()

        //getting the live data source from data source factory
        liveDataSource = cityDataSourceFactory.getItemLiveDataSource()

        //Getting PagedList config
        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPrefetchDistance(5)
            .setPageSize(10).build()

        //Building the paged list
        cityPagedList = LivePagedListBuilder(cityDataSourceFactory, pagedListConfig)
            .build()
        Log.d("abcdefgh","in viewmodel2")
    }
}