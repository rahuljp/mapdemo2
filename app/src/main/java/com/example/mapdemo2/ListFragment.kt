package com.example.mapdemo2

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mapdemo2.Network.Models.Data
import com.example.mapdemo2.Utills.log
import com.example.mapdemo2.ViewModels.CityViewModel
import com.example.mapdemo2.adapters.CityAdapter
import com.example.mapdemo2.adapters.vpadapter
import kotlinx.android.synthetic.main.fragment_list.view.*


class ListFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view= inflater.inflate(R.layout.fragment_list, container, false)
        view.mapfrag.setOnClickListener {
            (parentFragment as NavHostFragment).navController.navigate(R.id.action_listFragment_to_mapsFragment)
        }


        view.rv.layoutManager=LinearLayoutManager(this.context)
        view.rv.setHasFixedSize(true)


        var cityviewmodel=ViewModelProviders.of(this).get(CityViewModel :: class.java)
        var context=this.context
        context?.log(cityviewmodel.toString())


        var adapter= CityAdapter()
        cityviewmodel.cityPagedList?.observe(viewLifecycleOwner,object : Observer<PagedList<Data?>>{
            override fun onChanged(t: PagedList<Data?>?) {
                adapter.submitList(t)
                Log.d("abcdefgh","in listfragment onchanged")
            }


        })
        view.rv.adapter= adapter
        context?.log("adapter set")
        //view.rv.adapter=vpadapter(MainActivity.list,(parentFragment as NavHostFragment).navController)
        return view
    }

}