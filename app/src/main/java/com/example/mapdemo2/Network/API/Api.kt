package com.example.mapdemo2.Network.API

import com.example.mapdemo2.Network.Models.City
import com.example.mapdemo2.Network.Models.Data
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor


interface Api {



    @Headers("x-rapidapi-host: wft-geo-db.p.rapidapi.com",
        "x-rapidapi-key: b5416ad4b3msh5c69d9160d87c7ap10df3ejsn9dda7d0b7bc4")
    @GET("v1/geo/cities")
    fun getCities(@Query("offset") offset:String , @Query("limit") limit : String = "10") : Call<City>

    companion object{
        fun getApiClient() : Api{
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

            return Retrofit.Builder().baseUrl("https://wft-geo-db.p.rapidapi.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
                .create(Api :: class.java)
        }
    }
}