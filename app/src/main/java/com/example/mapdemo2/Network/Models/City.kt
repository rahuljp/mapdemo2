package com.example.mapdemo2.Network.Models

data class City(
    val data: List<Data>,
    val links: List<Link>,
    val metadata: Metadata
)