package com.example.mapdemo2.Network.Models

data class Metadata(
    val currentOffset: Int,
    val totalCount: Int
)