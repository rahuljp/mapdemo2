package com.example.mapdemo2.Network.Models

data class Link(
    val href: String,
    val rel: String
)