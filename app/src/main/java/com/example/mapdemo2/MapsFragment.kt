package com.example.mapdemo2



import com.example.mapdemo2.MainActivity

import android.graphics.Typeface
import androidx.fragment.app.Fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.paging.PagedList
import androidx.viewpager2.widget.ViewPager2
import com.example.mapdemo2.MapsFragmentArgs
import com.example.mapdemo2.Network.Models.Data
import com.example.mapdemo2.R
import com.example.mapdemo2.Screens.Adapters.vpadapter
import com.example.mapdemo2.Screens.Model.CityData
import com.example.mapdemo2.Utills.log
import com.example.mapdemo2.ViewModels.CityViewModel
import com.example.mapdemo2.adapters.CityAdapter

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_maps.view.*

class MapsFragment : Fragment() {

    private  var googleMap: GoogleMap? =null
    val args: MapsFragmentArgs by navArgs()
    var view2 : View? =null
    var lastpos : Int? =null
    var adapter : com.example.mapdemo2.Screens.Adapters.CityAdapter? = null
    var marker: Marker? = null


    private val callback = OnMapReadyCallback { googleMap ->
        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */

        this.googleMap=googleMap

        //updategooglemap(args.pos)
        googleMap.setMapStyle(this.context?.let { MapStyleOptions.loadRawResourceStyle(it,R.raw.style) })

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view=inflater.inflate(R.layout.fragment_maps, container, false)
        view2=view
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)

//        view.vp.adapter=vpadapter(MainActivity.list)
        var cityviewmodel= ViewModelProviders.of(this).get(CityViewModel :: class.java)
        adapter= com.example.mapdemo2.Screens.Adapters.CityAdapter()
        cityviewmodel.cityPagedList?.observe(viewLifecycleOwner,object :
            Observer<PagedList<Data?>> {
            override fun onChanged(t: PagedList<Data?>?) {
                adapter?.submitList(t)
                Log.d("abcdefgh","in listfragment onchanged")
            }


        })
        view.vp.adapter=adapter
        view.vp.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                this@MapsFragment.context?.log(position.toString())
                //updategooglemap(position)
                var d= adapter!!.currentList?.get(position) as Data
                updategooglemap(LatLng(d.latitude,d.longitude))
            }
        })
        view.listfrag.setOnClickListener {

            var controller = (parentFragment as NavHostFragment).navController
            controller.navigate(R.id.action_mapsFragment_to_listFragment)
        }



    }


//    fun updategooglemap(position : Int){
//
//
//        var data= MainActivity.list[position]
//
//        var markerOptions=MarkerOptions().position(data.location).title(data.title)
//        googleMap?.addMarker(markerOptions)
//        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(data.location,10f))
//        view2?.vp?.currentItem=position
//    }

    fun updategooglemap(latLng: LatLng){

        if (marker!=null){
            marker!!.remove()

        }


        var markerOptions=MarkerOptions().position(latLng)
        marker=googleMap?.addMarker(markerOptions)
        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,10f))
        //view2?.vp?.currentItem=position
    }
}