package com.example.mapdemo2.Screens.Adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mapdemo2.Network.Models.Data
import com.example.mapdemo2.R
import kotlinx.android.synthetic.main.vplist.view.*


class CityAdapter() : PagedListAdapter<Data,CityAdapter.ItemViewHolder?>(DIFF_CALLBACK) {



    lateinit var context : Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        context=parent.context
        var view= LayoutInflater.from(context).inflate(R.layout.vplist,parent,false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        Log.d("abcdefgh","in bindviewholder")
        Log.d("abcdefgh","${position}")
        Log.d("abcdefgh"," ${getItem(position)}")
        var view=holder.view
        var pos=holder.adapterPosition
        var data=getItem(position) as Data
        view.title2.text=data.city
        view.dist.text=data.country
        view.desc.text=data.region
        Glide.with(context).load(R.raw.abc).into(view.logo)
//        view.setOnClickListener{
//            var action= ListFragmentDirections.actionListFragmentToMapsFragment(pos)
//            controller.navigate(action)
//        }
    }

    class ItemViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var view = itemView
    }

    companion object {
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<Data> =
            object : DiffUtil.ItemCallback<Data>() {
                override fun areItemsTheSame(
                    oldItem: Data,
                    newItem: Data
                ): Boolean {
                    Log.d("abcdefgh","in areitemsame")
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: Data,
                    newItem: Data
                ): Boolean {
                    Log.d("abcdefgh","in arecontainsame")
                    return oldItem == newItem
                }
            }
    }
}