package com.example.mapdemo2.Screens.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mapdemo2.R
import com.example.mapdemo2.Screens.Model.CityData
import kotlinx.android.synthetic.main.vplist.view.*

class vpadapter(list : ArrayList<CityData>) : RecyclerView.Adapter<vpadapter.VH>(){
    var list: ArrayList<CityData> = list
    lateinit var context : Context

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var view = itemView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        context=parent.context
        var view=LayoutInflater.from(context).inflate(R.layout.vplist,parent,false)
        return VH(view)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        var view=holder.view
       var pos=holder.adapterPosition
        var data=list[pos]
        view.title2.text=data.title
        view.dist.text=data.distance
        view.desc.text=data.disc
        Glide.with(context).load(data.image).into(view.logo)
        //todo : add logo

    }

    override fun getItemCount(): Int {
        return list.size
    }
}