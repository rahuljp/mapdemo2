package com.example.mapdemo2.Screens.Model

import com.google.android.gms.maps.model.LatLng

data class CityData(val title : String, val distance : String, val disc : String, val location : LatLng,
                    val image : String)
