package com.example.mapdemo2.DataSources

import android.content.ClipData
import com.example.mapdemo2.Network.Models.Data
import javax.sql.DataSource
import androidx.lifecycle.MutableLiveData

import android.content.ClipData.Item

import androidx.paging.PageKeyedDataSource




class CityDataSourceFactory : androidx.paging.DataSource.Factory<Integer, Data>() {
    private val cityLiveDataSource = MutableLiveData<PageKeyedDataSource<Integer, Data>>()

    override fun create(): androidx.paging.DataSource<Integer, Data> {
        var citydata=CityDataSource()
        cityLiveDataSource.postValue(citydata)
        return citydata
    }

    fun getItemLiveDataSource(): MutableLiveData<PageKeyedDataSource<Integer, Data>> {
        return cityLiveDataSource
    }
}