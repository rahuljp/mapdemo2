package com.example.mapdemo2.DataSources

import android.util.Log
import androidx.paging.PageKeyedDataSource
import com.example.mapdemo2.Network.API.Api
import com.example.mapdemo2.Network.Models.City
import com.example.mapdemo2.Network.Models.Data
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CityDataSource : PageKeyedDataSource<Integer, Data>() {
    override fun loadInitial(
        params: LoadInitialParams<Integer>,
        callback: LoadInitialCallback<Integer, Data>
    ) {
        Api.getApiClient().getCities("0").enqueue(object : Callback<City>{
            override fun onResponse(call: Call<City>, response: Response<City>) {
                if (!response.isSuccessful){
                    return
                }

                Log.d("abcdefgh","in loadinitial")
                var data = response.body() as City
                callback.onResult(data.data,null, Integer(data.metadata.currentOffset+10))
                Log.d("abcdefgh","in loadinitial $data")

            }

            override fun onFailure(call: Call<City>, t: Throwable) {
                TODO("Not yet implemented")
            }

        })
    }

    override fun loadBefore(params: LoadParams<Integer>, callback: LoadCallback<Integer, Data>) {

        Api.getApiClient().getCities(params.key.toInt().toString()).enqueue(object : Callback<City>{
            override fun onResponse(call: Call<City>, response: Response<City>) {
                if (!response.isSuccessful){
                    return
                }
                Log.d("abcdefgh","in loadbefore")
                var data = response.body() as City
                var key=if (params.key.toInt()>10) params.key.toInt()-10 else null
                callback.onResult(data.data, key?.let { Integer(it) })
            }

            override fun onFailure(call: Call<City>, t: Throwable) {
                TODO("Not yet implemented")
            }

        })

    }

    override fun loadAfter(params: LoadParams<Integer>, callback: LoadCallback<Integer, Data>) {
        Api.getApiClient().getCities(params.key.toInt().toString(),"10").enqueue(object : Callback<City>{
            override fun onResponse(call: Call<City>, response: Response<City>) {
                if (!response.isSuccessful){
                    return
                }
                Log.d("abcdefgh","in loadafter current ${params.key}")
                var data = response.body() as City
                var key=if (params.key.toInt()<data.metadata.totalCount-10) params.key.toInt()+10 else null
                Log.d("abcdefgh","in loadafter then ${key}")
                Log.d("data",data.data[0].city)
                callback.onResult(data.data, key?.let { Integer(it) })
            }

            override fun onFailure(call: Call<City>, t: Throwable) {
                TODO("Not yet implemented")
            }

        })
    }

}